# Carrefour Java kata -- Delivery -- Solution

Ce projet est une proposition de solution pour le Carrefour Java kata -- Delivery.

## Pré requis

- Java Development Kit (JDK) >= 21
- Gradle
- Docker

## Démarrage

1. Cloner le dépôt:

   ```bash
   git clone https://gitlab.com/Toumani/carrefour-java-kata.git
   ```

2. Naviguer vers le dépôt:

   ```bash
   cd carrefour-java-kata
   ```

3. Builder le projet avec Gradle:

   ```bash
   ./gradlew run
   ```

OU

1. Builder l'image Docker:

   ```bash
   docker build -t carrefour-java-kata .
   ```

2. Exécuter le conteneur Docker:

   ```bash
   docker run -p 8080:8080 carrefour-java-kata
   ```

   L'application est accessible à l'adresse: `http://localhost:8080`.

## Implémentation

Étant donné les containtes de temps, seuls les fontionnalités de récupération des types de livraison et enregistrement de nouvelles livraisons sont implémentées.\
L'implémentation effectue une vérification sur le créneau de livraison lors de l'enregistrement pour donner du sens au type de livraison.
Une règle de gestion arbitraire contraint l'utilisateur à choisir le créneau de livraison en fonction du mode de livraision.
