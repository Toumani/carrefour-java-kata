FROM eclipse-temurin:latest

WORKDIR /app

COPY build.gradle.kts settings.gradle.kts /app/

COPY . .

RUN ./gradlew build

CMD ["java", "-jar", "build/libs/carrefour-java-kata-0.0.1-SNAPSHOT.jar"]