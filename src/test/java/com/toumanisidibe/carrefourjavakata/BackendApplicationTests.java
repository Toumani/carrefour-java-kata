package com.toumanisidibe.carrefourjavakata;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BackendApplicationTests {

	@LocalServerPort
	private int port;

	private final TestRestTemplate restTemplate = new TestRestTemplate();

	@Test
	void shouldGetAllDeliveryModes() {
		String url = "http://localhost:" + port + "/deliveries/modes";
		List<String> expectedResponse = asList("DRIVE", "DELIVERY", "DELIVERY_TODAY", "DELIVERY_ASAP");

		List<String> actualResponse = restTemplate.getForObject(url, List.class);

		// Perform assertions
		assertEquals(expectedResponse, actualResponse, "Response from the endpoint does not match the expected value");
	}

	@Test
	void shouldSaveTheDelivery() {
		String url = "http://localhost:" + port + "/deliveries";

		var now = LocalDateTime.now();
		Delivery delivery = Delivery.builder()
				.mode(DeliveryMode.DELIVERY_ASAP)
				.lowerTime(now.plusHours(1))
				.upperTime(now.plusHours(2))
				.build();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Delivery> requestEntity = new HttpEntity<>(delivery, headers);

		Delivery actualResponse = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Delivery.class).getBody();

		assertNotNull(actualResponse);
		assertNotNull(actualResponse.getId());
		assertEquals(actualResponse.getMode(), delivery.getMode());
		assertEquals(actualResponse.getLowerTime(), delivery.getLowerTime());
		assertEquals(actualResponse.getUpperTime(), delivery.getUpperTime());
	}

	@Test
	void shouldRejectTheDelivery() {
		String url = "http://localhost:" + port + "/deliveries";

		var now = LocalDateTime.now();
		Delivery badDelivery = Delivery.builder()
				.mode(DeliveryMode.DRIVE)
				.lowerTime(now.plusHours(1))
				.upperTime(now.plusHours(2))
				.build();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<Delivery> requestEntity = new HttpEntity<>(badDelivery, headers);

		Delivery actualResponse = restTemplate.exchange(url, HttpMethod.POST, requestEntity, Delivery.class).getBody();

		assertNull(actualResponse);
	}
}
