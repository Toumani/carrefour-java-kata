package com.toumanisidibe.carrefourjavakata;

public class DeliveryException extends RuntimeException {
    public DeliveryException(String message) {
        super(message);
    }
}