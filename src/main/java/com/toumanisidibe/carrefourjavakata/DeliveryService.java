package com.toumanisidibe.carrefourjavakata;

public interface DeliveryService {
    Delivery save(Delivery delivery);
}
