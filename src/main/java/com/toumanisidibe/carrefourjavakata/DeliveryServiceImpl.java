package com.toumanisidibe.carrefourjavakata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DeliveryServiceImpl implements DeliveryService {
    private final DeliveryRepository deliveryRepository;

    @Autowired
    public DeliveryServiceImpl(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public Delivery save(Delivery delivery) {
        // let's check if the delivery is valid
        // validation rules are as follow:
        // - if mode == DRIVE, delivery must be at least 4 days from today
        // - if mode == DELIVERY, delivery must be at least 2 days from today
        // - if mode == DELIVERY_TODAY, delivery must at least 6 hours from now
        // - if mode == DELIVERY_ASAP, delivery can be at any time
        // and obviously lower time must come before upper time

        if (delivery.getLowerTime().isAfter(delivery.getUpperTime()))
            throw new DeliveryException("Lower time cannot be after upper time");

        var now = LocalDateTime.now();
        var isDeliveryValid = switch (delivery.getMode()) {
            case DRIVE -> delivery.getLowerTime().isAfter(now.plusDays(4));
            case DELIVERY -> delivery.getLowerTime().isAfter(now.plusDays(2));
            case DELIVERY_TODAY -> delivery.getLowerTime().isAfter(now.plusHours(6));
            case DELIVERY_ASAP -> true;
        };
        if (!isDeliveryValid)
            throw new DeliveryException("Specified lower time not suitable with delivery mode");

        return deliveryRepository.save(delivery);
    }
}
