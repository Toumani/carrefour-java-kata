package com.toumanisidibe.carrefourjavakata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/deliveries")
public class DeliveryController {
    private final DeliveryService deliveryService;

    @Autowired
    public DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @GetMapping("/modes")
    public ResponseEntity<List<String>> getDeliveryModes() {
        return ResponseEntity.ok(
                Arrays.stream(DeliveryMode.values()).map(Enum::name).toList()
        );
    }

    @PostMapping
    public ResponseEntity<Delivery> saveDelivery(@RequestBody Delivery delivery) {
        var savedDelivery = deliveryService.save(delivery);
        return ResponseEntity.ok(savedDelivery);
    }

    @ExceptionHandler(DeliveryException.class)
    public ResponseEntity<?> handleStorageFileNotFound(DeliveryException exc) {
        return ResponseEntity.status(409).build(); // Conflict
    }
}
