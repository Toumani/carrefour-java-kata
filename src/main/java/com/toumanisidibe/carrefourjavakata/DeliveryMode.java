package com.toumanisidibe.carrefourjavakata;

public enum DeliveryMode {
    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP
}
